curl -X POST http://localhost:8080/v1/api/books \
    -H "Content-Type: application/json" \
    -d '{"name": "Old Book", "author": "Old Author", "isbn": "f542854f-8f5a-4154-a595-5f854354854d", "price": 44.44}'


curl -X PUT -H "Content-Type: application/json" -d '{"isbn":"f542854f-8f5a-4154-a595-5f854354854d","name":"New Book","author":"New Author","price":"55.55"}' http://localhost:8080/v1/api/books/f542854f-8f5a-4154-a595-5f854354854d

curl -X GET 'http://localhost:8080/v1/api/books/f542854f-8f5a-4154-a595-5f854354854d'

curl -X GET 'http://localhost:8080/v1/api/books?isbn=f542854f-8f5a-4154-a595-5f854354854d'

curl -X GET 'http://localhost:8080/v1/api/books?isbn=f542854f-8f5a-4154-a595-5f854354854d&author=New%20Author'

curl -X GET 'http://localhost:8080/v1/api/books?name=New%20Book&isbn=f542854f-8f5a-4154-a595-5f854354854d&author=New%20Author'

curl -X DELETE http://localhost:8080/v1/api/books/f542854f-8f5a-4154-a595-5f854354854d
