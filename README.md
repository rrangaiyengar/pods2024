## Getting Started 
git clone https://gitlab.com/rrangaiyengar/pods2024.git

(Rename the folder to book-service)

## Move to the book-service directory 
cd book-service

## To build the service
mvn clean install 

## Sample CURL commands to start Docker
sh ./docker-demo.sh

## In a separate terminal sample cURLs to run the Book-Service
sh ./book-api-calls.sh

## After code changes 
git add <changed-files>

## Local commit
git commit -m "Commit Message"

## To push your changes to the repo
git push -uf origin main

