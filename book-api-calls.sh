#!/bin/bash

# Create a new book
echo "Create a new book"
curl -X POST http://localhost:8080/v1/api/books \
    -H "Content-Type: application/json" \
    -d '{"name": "New Book", "author": "John Doe", "isbn": "f542854f-8f5a-4154-a595-5f854354854d", "price": 44.44}'
printf "\n"
# Update a book
echo "Update a book"
curl -X PUT -H "Content-Type: application/json" -d '{"isbn":"f542854f-8f5a-4154-a595-5f854354854d","name":"Updated Book Title","author":"John Doe","price":"55.55"}' http://localhost:8080/v1/api/books/f542854f-8f5a-4154-a595-5f854354854d
printf "\n"

#Get the book id through path
echo "Get a book"
curl -X GET 'http://localhost:8080/v1/api/books/f542854f-8f5a-4154-a595-5f854354854d'
printf "\n"
# Search for books by ISBN
echo "Search a book by ISBN"
curl -X GET 'http://localhost:8080/v1/api/books?isbn=f542854f-8f5a-4154-a595-5f854354854d'
printf "\n"
# Search for books by ISBN and author
echo "Search for books by ISBN and author"
curl -X GET 'http://localhost:8080/v1/api/books?isbn=f542854f-8f5a-4154-a595-5f854354854d&author=John%20Doe'
printf "\n"

# Search for books by name, ISBN, and author
echo "Search for books by name, ISBN, and author"
curl -X GET 'http://localhost:8080/v1/api/books?name=Updated%20Book%20Title&isbn=f542854f-8f5a-4154-a595-5f854354854d&author=John%20Doe'
printf "\n"
# Delete a book
echo "Delete a book"
curl -X DELETE http://localhost:8080/v1/api/books/f542854f-8f5a-4154-a595-5f854354854d
printf "\n"
#Try getting the book again - you should get an empty set
echo "After Deleting no book present"
curl -X GET 'http://localhost:8080/v1/api/books?isbn=f542854f-8f5a-4154-a595-5f854354854d'
printf "\n"