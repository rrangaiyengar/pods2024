package com.iisc.pods.bookservice.model;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.Data;

@Entity
@Data
public class Book {
    @Id
    private String isbn;
    private String name;
    private String author;
    private Double price;
}
