package com.iisc.pods.bookservice.exception;

import org.springframework.http.HttpStatus;

import java.util.UUID;

public class BookNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    private final String isbn;

    private HttpStatus status;

    public BookNotFoundException(String isbn) {
        super("Book not found with ISBN: " + isbn);
        this.isbn = isbn;
        this.status = HttpStatus.NOT_FOUND;
    }

    public String getIsbn() {
        return isbn;
    }
}
