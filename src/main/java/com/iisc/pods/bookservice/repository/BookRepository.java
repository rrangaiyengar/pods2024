package com.iisc.pods.bookservice.repository;

import com.iisc.pods.bookservice.model.Book;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface BookRepository extends JpaRepository<Book, String> {

    @Query("SELECT b FROM Book b WHERE b.name LIKE %:name% AND b.isbn = :isbn AND b.author = :author")
    List<Book> findByNameContainingAndIsbnAndAuthor(String name, String isbn, String author);

    @Query("SELECT b FROM Book b WHERE b.name LIKE %:name% AND b.author = :author")
    List<Book> findByNameContainingAndAuthor(String name, String author);

    @Query("SELECT b FROM Book b WHERE b.isbn = :isbn AND b.author = :author")
    List<Book> findByIsbnAndAuthor(String isbn, String author);

    @Query("SELECT b FROM Book b WHERE b.name LIKE %:name%")
    List<Book> findByNameContaining(String name);

    @Query("SELECT b FROM Book b WHERE b.isbn = :isbn")
    List<Book> findByIsbn(String isbn);

    @Query("SELECT b FROM Book b WHERE b.author = :author")
    List<Book> findByAuthor(String author);

    @Transactional
    @Modifying
    @Query("DELETE FROM Book b WHERE b.isbn = :isbn")
    void deleteByIsbn(String isbn);
}
